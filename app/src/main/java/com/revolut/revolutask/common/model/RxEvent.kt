package com.revolut.revolutask.common.model

import androidx.lifecycle.Observer

open class RxEvent<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}

/**
 * An [Observer] for [RxEvent]s, simplifying the pattern of checking if the [RxEvent]'s content has
 * already been handled.
 *
 * [onEventUnhandledContent] is *only* called if the [RxEvent]'s contents has not been handled.
 */
class EventObserver<T>(private val onEventUnhandledContent: (T) -> Unit) : Observer<RxEvent<T>> {
    override fun onChanged(event: RxEvent<T>?) {
        event?.getContentIfNotHandled()?.let { value ->
            onEventUnhandledContent(value)
        }
    }
}
