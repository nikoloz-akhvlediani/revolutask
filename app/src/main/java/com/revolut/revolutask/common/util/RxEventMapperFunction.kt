package com.revolut.revolutask.common.util

import com.revolut.revolutask.common.model.RxEvent

open class RxEventMapperFunction<U, D>(
    private val mapperFunction: MapperFunction<U, D>
) : MapperFunction<U, RxEvent<D>> {

    override fun map(value: U): RxEvent<D> =
        RxEvent(mapperFunction.map(value))

}