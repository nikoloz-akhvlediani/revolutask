package com.revolut.revolutask.common.di

import com.revolut.revolutask.common.rx.BaseSchedulerProvider
import com.revolut.revolutask.common.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class SchedulersModule {

    @Provides
    fun provideSchedulerProvider(schedulerProvider: SchedulerProvider): BaseSchedulerProvider =
        schedulerProvider

}