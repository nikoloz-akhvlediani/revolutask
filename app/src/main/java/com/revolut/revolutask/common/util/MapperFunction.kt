package com.revolut.revolutask.common.util

interface MapperFunction<U, D> {

    fun map(value: U): D

}