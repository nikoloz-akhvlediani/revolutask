package com.revolut.revolutask.presentation.screens.home

data class UpdateCurrencyDataModel(
    val currencyRateList: List<CurrencyRate>,
    val enteredBaseValue: Double
)