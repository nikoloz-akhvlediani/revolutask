package com.revolut.revolutask.presentation.mvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import com.revolut.revolutask.common.util.MapperFunction
import com.revolut.revolutask.presentation.mvvm.model.RxResult
import io.reactivex.ObservableOperator
import io.reactivex.Observer
import io.reactivex.observers.DisposableObserver

class UseCaseResultToViewModelOperator<D, U>(
    private val liveData: MutableLiveData<RxResult<D>>,
    private val function: MapperFunction<U, D>
) : ObservableOperator<RxResult<U>, RxResult<U>> {

    override fun apply(observer: Observer<in RxResult<U>>): Observer<in RxResult<U>> {
        return LiveDataMappingObserver()
    }

    inner class LiveDataMappingObserver : DisposableObserver<RxResult<U>>() {

        override fun onComplete() {
        }

        override fun onNext(t: RxResult<U>) {
            when (t) {
                is RxResult.Success -> {
                    liveData.postValue(
                        RxResult.Success(function.map(t.data))
                    )
                }
                is RxResult.Error -> {
                    liveData.postValue(t)
                }
                is RxResult.Loading -> {
                    liveData.postValue(t)
                }
            }
        }

        override fun onError(e: Throwable) {
            liveData.postValue(RxResult.Error(e))
        }

    }
}