package com.revolut.revolutask.presentation.screens.home

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.revolut.revolutask.R
import com.revolut.revolutask.common.model.RxEvent
import com.revolut.revolutask.presentation.extensions.applyBundle
import com.revolut.revolutask.presentation.extensions.bindView
import com.revolut.revolutask.presentation.mvvm.view.BaseFragmentWithViewModel
import com.revolut.revolutask.presentation.util.KeyboardEventListener

class CurrencyRatesFragment : BaseFragmentWithViewModel<CurrencyRatesViewModel>(),
    CurrencyStateEventListener, KeyboardEventListener {

    val recyclerView : RecyclerView by bindView(R.id.recycler_view)

    override val layoutRes: Int = R.layout.fragment_currency_rates

    var adapter: CurrencyRateListAdapter? = null

    override fun onViewModelCreated(viewModel: CurrencyRatesViewModel) {
        super.onViewModelCreated(viewModel)
        arguments?.getString(KEY_BASE_CURRENCY)?.let {
            viewModel.baseCurrency = it
        }
        viewModel.currencyRatesLiveData.observe(
            this,
            object : FullScreenLiveDataObserver<RxEvent<List<CurrencyRate>>>() {

                override fun handleSuccessResult(result: RxEvent<List<CurrencyRate>>) {
                    result.getContentIfNotHandled()?.let {
                        adapter = CurrencyRateListAdapter(
                            it,
                            this@CurrencyRatesFragment,
                            this@CurrencyRatesFragment
                        )
                        recyclerView.layoutManager = LinearLayoutManager(context)
                        recyclerView.adapter = adapter
                    }
                }

            }
        )
        viewModel.updatedCurrencyRatesLiveData.observe(
            this,
            object : SimpleLiveDataObserver<RxEvent<UpdateCurrencyDataModel>>() {
                override fun handleSuccessResult(result: RxEvent<UpdateCurrencyDataModel>) {
                    result.getContentIfNotHandled()?.let {
                        adapter?.updateData(it.currencyRateList, it.enteredBaseValue)
                    }
                }
            }
        )
        viewModel.selectCurrencyLiveData.observe(
            this,
            Observer {
                it.getContentIfNotHandled()?.let {
                    adapter?.selectCurrencyRate(it)
                    recyclerView.scrollToPosition(0)
                }
            }
        )
    }

    override fun onEnteredValueUpdated(enteredValue: Double) {
        viewModel.updateEnteredValue(enteredValue)
    }

    override fun onCurrencySelected(currencyRate: CurrencyRate) {
        viewModel.selectCurrency(currencyRate)
    }

    override fun onRetry() {
        super.onRetry()
        viewModel.getCurrencyRates()
    }

    override fun onHideKeyboard() {
        activity?.let { activity ->
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.currentFocus
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    companion object {

        private const val KEY_BASE_CURRENCY = "baseCurrency"

        fun createInstance(baseCurrency: String) =
            CurrencyRatesFragment().applyBundle {
                putString(KEY_BASE_CURRENCY, baseCurrency)
            }

    }

}