package com.revolut.revolutask.presentation.base.fragment

interface RetryListener {

    fun onRetry()

}