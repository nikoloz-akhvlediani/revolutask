package com.revolut.revolutask.presentation.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.AnimRes
import androidx.fragment.app.Fragment
import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment(), RetryListener {

    private var loaderFragment: LoaderFragment? = null
    private var errorFragment: ErrorFragment? = null
    val fragmentTag: String = this::class.java.name

    abstract val layoutRes: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val tmpView = inflater.inflate(layoutRes, container, false)
        val view = FrameLayout(context!!)
        view.addView(tmpView)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (view.id == View.NO_ID) {
            view.id = View.generateViewId()
        }
        arguments?.let(::parseBundle)
        initViews(view)
    }

    protected open fun parseBundle(bundle: Bundle) {}

    protected open fun initViews(view: View) {}

    protected fun showFullScreenLoaderFragment() {
        view?.let { view ->
            loaderFragment = LoaderFragment.createInstance().also {
                replaceFragment(view.id, it)
            }
        }
    }

    protected fun hideFullScreenLoaderFragment() {
        loaderFragment?.let(::removeFragment)
    }

    protected fun showFullScreenError(errorModel: ErrorFragment.ErrorModel) {
        view?.let { view ->
            errorFragment = ErrorFragment.createInstance(errorModel).also {
                it.retryListener = this
                replaceFragment(view.id, it)
            }
        }
    }

    protected fun hideErrorFragment() {
        errorFragment?.let(::removeFragment)
    }

    protected fun replaceFragment(
        id: Int? = null,
        fragment: Fragment,
        @AnimRes animIn: Int? = null,
        @AnimRes animOut: Int? = null
    ) {
        val ft = childFragmentManager.beginTransaction()
        if (animIn != null && animOut != null)
            ft.setCustomAnimations(animIn, animOut)
        ft.replace(id ?: view!!.id, fragment, fragment::class.java.simpleName)
            .commitAllowingStateLoss()
    }

    protected fun removeFragment(fragment: Fragment) {
        childFragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss()
    }

    override fun onRetry() {

    }

}