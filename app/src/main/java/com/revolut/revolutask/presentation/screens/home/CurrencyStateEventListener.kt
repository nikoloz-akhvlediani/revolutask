package com.revolut.revolutask.presentation.screens.home

interface CurrencyStateEventListener {

    fun onEnteredValueUpdated(enteredValue: Double)

    fun onCurrencySelected(currencyRate: CurrencyRate)

}