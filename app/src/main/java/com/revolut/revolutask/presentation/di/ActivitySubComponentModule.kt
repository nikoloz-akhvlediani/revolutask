package com.revolut.revolutask.presentation.di

import com.revolut.revolutask.presentation.screens.home.CurrencyRatesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivitySubComponentModule {

    @ContributesAndroidInjector
    fun contributeCurrencyRatesActivity(): CurrencyRatesActivity

}