package com.revolut.revolutask.presentation.screens.home

import com.revolut.revolutask.R
import com.revolut.revolutask.presentation.base.activity.BaseFragmentActivity

class CurrencyRatesActivity : BaseFragmentActivity<CurrencyRatesFragment>() {

    override val fragmentContainerRes: Int = R.id.fragment_container
    override val layoutRes: Int = R.layout.activity_currency_rates

    override fun initFragment(): CurrencyRatesFragment = CurrencyRatesFragment.createInstance("EUR")

}
