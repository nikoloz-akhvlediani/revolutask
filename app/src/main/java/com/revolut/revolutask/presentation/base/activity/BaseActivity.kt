package com.revolut.revolutask.presentation.base.activity

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.revolut.revolutask.presentation.base.fragment.BaseFragment
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {

    abstract val layoutRes: Int

    protected fun replaceFragment(mainFragmentId: Int, baseFragment: BaseFragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(mainFragmentId, baseFragment, baseFragment::class.java.simpleName)
            .commitAllowingStateLoss()
    }

    protected fun replaceFragment(mainFragmentId: Int, baseFragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(mainFragmentId, baseFragment, baseFragment::class.java.simpleName)
            .commitAllowingStateLoss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        intent?.let {
            parseIntent(it)
        }
    }

    protected open fun parseIntent(intent: Intent) {}

}