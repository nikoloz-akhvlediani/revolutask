package com.revolut.revolutask.presentation.di

import com.revolut.revolutask.presentation.base.fragment.ErrorFragment
import com.revolut.revolutask.presentation.base.fragment.LoaderFragment
import com.revolut.revolutask.presentation.screens.home.CurrencyRatesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentSubComponentModule {

    @ContributesAndroidInjector
    fun contributeCurrencyRatesFragment(): CurrencyRatesFragment

    @ContributesAndroidInjector
    fun contributeLoaderFragment(): LoaderFragment

    @ContributesAndroidInjector
    fun contributeErrorFragment(): ErrorFragment

}