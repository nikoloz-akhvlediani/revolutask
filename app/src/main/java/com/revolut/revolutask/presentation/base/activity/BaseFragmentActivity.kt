package com.revolut.revolutask.presentation.base.activity

import android.os.Bundle
import com.revolut.revolutask.presentation.base.fragment.BaseFragment


abstract class BaseFragmentActivity<F : BaseFragment> : BaseActivity() {

    protected lateinit var currentFragment: F

    abstract val fragmentContainerRes: Int

    abstract fun initFragment() : F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentFragment = initFragment()
        replaceFragment(fragmentContainerRes, currentFragment)
    }

}