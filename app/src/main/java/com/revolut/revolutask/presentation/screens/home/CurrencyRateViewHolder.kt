package com.revolut.revolutask.presentation.screens.home

import android.text.Editable
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mynameismidori.currencypicker.ExtendedCurrency
import com.revolut.revolutask.R
import com.revolut.revolutask.presentation.extensions.format
import com.revolut.revolutask.presentation.util.KeyboardEventListener
import com.revolut.revolutask.presentation.util.SimpleTextWatcher

class CurrencyRateViewHolder(
    itemView: View,
    private var currencyStateEventListener: CurrencyStateEventListener,
    private val keyboardEventListener: KeyboardEventListener
) : RecyclerView.ViewHolder(itemView) {

    private val imageView = itemView.findViewById<ImageView>(R.id.image_view)
    private val titleTextView = itemView.findViewById<TextView>(R.id.title_text_view)
    private val subTitleTextView = itemView.findViewById<TextView>(R.id.sub_title_text_view)
    private val editText = itemView.findViewById<EditText>(R.id.edit_text)

    private var currencyRate: CurrencyRate? = null
    private var selected: Boolean = false

    fun setContent(currencyRate: CurrencyRate, selected: Boolean, enteredBaseValue: Double) {
        this.currencyRate = currencyRate
        this.selected = selected
        with(currencyRate) {
            titleTextView.text = currency
            setCurrencyDetails(this)
            updateValueOfEditText(enteredBaseValue, rate)
        }
        setViewListeners()
        if (selected) {
            if (!editText.isFocused) {
                editText.requestFocus()
            }
        }
    }

    private fun setViewListeners() {
        editText.setOnFocusChangeListener { view, focused ->
            if (!selected && focused) {
                currencyRate?.let(currencyStateEventListener::onCurrencySelected)
            }
        }
        itemView.setOnClickListener {
            if (!selected)
                currencyRate?.let(currencyStateEventListener::onCurrencySelected)
        }
        editText.addTextChangedListener(textWatcher)
    }

    private fun setCurrencyDetails(currencyRate: CurrencyRate) {
        val extendedCurrency = ExtendedCurrency.getCurrencyByISO(currencyRate.currency)
        subTitleTextView.text = extendedCurrency.name
        imageView.setImageResource(extendedCurrency.flag)
    }

    fun updateData(currencyPayload: CurrencyRateListAdapter.CurrencyPayload) {
        with(currencyPayload) {
            currencyRate?.rate = newRate
            updateValueOfEditText(enteredValue, newRate)
        }
    }

    private fun updateValueOfEditText(enteredAmount: Double, rate: Double) {
        editText.setText((enteredAmount * rate).format(2))
    }

    private fun getAmount(): Double {
        return try {
            editText.text.toString().toDouble()
        } catch (e: Exception) {
            0.0
        }
    }

    fun clear() {
        if (editText.isFocused)
            keyboardEventListener.onHideKeyboard()
        editText.removeTextChangedListener(textWatcher)
        editText.onFocusChangeListener = null
        itemView.setOnClickListener(null)
        currencyRate = null
        selected = false
    }


    private val textWatcher = object : SimpleTextWatcher() {
        override fun afterTextChanged(p0: Editable?) {
            if (selected)
                currencyStateEventListener.onEnteredValueUpdated(getAmount())
        }
    }

}