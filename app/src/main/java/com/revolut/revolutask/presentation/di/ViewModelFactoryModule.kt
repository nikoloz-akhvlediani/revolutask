package com.revolut.revolutask.presentation.di

import androidx.lifecycle.ViewModelProvider
import com.revolut.revolutask.presentation.mvvm.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}