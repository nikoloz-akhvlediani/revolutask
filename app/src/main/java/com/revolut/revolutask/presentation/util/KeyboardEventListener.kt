package com.revolut.revolutask.presentation.util

interface KeyboardEventListener {

    fun onHideKeyboard()

}