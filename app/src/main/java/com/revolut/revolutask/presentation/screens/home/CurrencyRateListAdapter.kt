package com.revolut.revolutask.presentation.screens.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.revolut.revolutask.R
import com.revolut.revolutask.presentation.util.KeyboardEventListener

class CurrencyRateListAdapter(
    private val currencyRateList: List<CurrencyRate>,
    private var currencyStateEventListener: CurrencyStateEventListener,
    private val keyboardEventListener: KeyboardEventListener
) : RecyclerView.Adapter<CurrencyRateViewHolder>() {

    private var enteredBaseValue = 1.0

    override fun getItemCount(): Int = currencyRateList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CurrencyRateViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_currency_rate,
                parent,
                false
            ),
            currencyStateEventListener,
            keyboardEventListener
        )

    override fun onBindViewHolder(holder: CurrencyRateViewHolder, position: Int) {
        holder.setContent(currencyRateList[position], position == 0, enteredBaseValue)
    }

    override fun onBindViewHolder(
        holder: CurrencyRateViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            val list = payloads[0]
            if (list is List<*>) {
                val payload = list[position]
                if (payload is CurrencyPayload) {
                    holder.updateData(payload)
                }
            }
        }
    }

    override fun onViewDetachedFromWindow(holder: CurrencyRateViewHolder) {
        holder.clear()
        super.onViewDetachedFromWindow(holder)
    }

    fun updateData(currencyRateList: List<CurrencyRate>, enteredBaseValue: Double) {
        this.enteredBaseValue = enteredBaseValue
        notifyItemRangeChanged(
            1,
            currencyRateList.size - 1,
            currencyRateList.map {
                CurrencyPayload(
                    enteredBaseValue,
                    it.rate
                )
            }
        )
    }

    fun selectCurrencyRate(index: Int) {
        if (index != -1) {
            notifyItemMoved(index, 0)
            notifyItemChanged(0)
            notifyItemChanged(1)
        }
    }

    data class CurrencyPayload(
        val enteredValue: Double,
        val newRate: Double
    )

}