package com.revolut.revolutask.presentation.base.fragment

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.revolut.revolutask.R
import com.revolut.revolutask.presentation.extensions.applyBundle
import java.io.Serializable

class ErrorFragment : BaseFragment() {

    private lateinit var imageView: ImageView
    private lateinit var textView: TextView
    private lateinit var retryButton: View

    var retryListener: RetryListener? = null
    private var errorModel: ErrorModel? = null

    override val layoutRes: Int
        get() = R.layout.fragment_error

    override fun parseBundle(bundle: Bundle) {
        super.parseBundle(bundle)
        errorModel = bundle.getSerializable(KEY_ERROR_MODEL) as ErrorModel?
    }

    override fun initViews(view: View) {
        super.initViews(view)
        retryButton = view.findViewById(R.id.retry_button)
        imageView = view.findViewById(R.id.image_view)
        textView = view.findViewById(R.id.text_view)
        retryButton.setOnClickListener {
            retryListener?.onRetry()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (errorModel?.errorType) {
            ErrorType.NETWORK -> {
                imageView.setImageResource(R.drawable.ic_error_network)
                textView.setText(R.string.network_error)
            }
            ErrorType.OTHER -> {
                imageView.setImageResource(R.drawable.ic_unexpected_error)
                textView.text = errorModel?.errorMessage ?: getString(R.string.unexpected_error)
            }
            else -> {
            }
        }
    }

    class ErrorModel(
        val errorType: ErrorType,
        val errorMessage: String? = null
    ) : Serializable

    enum class ErrorType {
        NETWORK, OTHER
    }

    companion object {

        private const val KEY_ERROR_MODEL = "errorModel"

        fun createInstance(errorModel: ErrorModel) =
            ErrorFragment().applyBundle {
                putSerializable(KEY_ERROR_MODEL, errorModel)
            }

    }

}
