package com.revolut.revolutask.presentation.di

import android.content.Context
import com.revolut.revolutask.presentation.RevolutaskApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideAppContent(revolutaskApp: RevolutaskApp): Context =
        revolutaskApp

}