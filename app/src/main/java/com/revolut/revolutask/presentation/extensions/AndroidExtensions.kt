package com.revolut.revolutask.presentation.extensions

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

inline fun <T : Fragment> T.applyBundle(block: Bundle.() -> Unit): T {
    val bundle = Bundle()
    block(bundle)
    this.arguments = bundle
    return this
}

fun Context.getCompatColor(@ColorRes color: Int) =
    ContextCompat.getColor(this, color)


fun Context?.showToast(message: String?) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Double.format(digits: Int) = "%.${digits}f".format(this)