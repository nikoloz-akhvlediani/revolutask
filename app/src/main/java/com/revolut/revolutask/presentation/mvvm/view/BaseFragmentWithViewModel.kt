package com.revolut.revolutask.presentation.mvvm.view

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.revolut.revolutask.R
import com.revolut.revolutask.data.exception.NoNetworkException
import com.revolut.revolutask.presentation.base.fragment.BaseFragment
import com.revolut.revolutask.presentation.base.fragment.ErrorFragment
import com.revolut.revolutask.presentation.mvvm.model.RxResult
import com.revolut.revolutask.presentation.mvvm.viewmodel.BaseViewModel
import com.revolut.revolutask.presentation.mvvm.viewmodel.ViewModelFactory
import java.lang.reflect.ParameterizedType
import javax.inject.Inject


abstract class BaseFragmentWithViewModel<VM : BaseViewModel> : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    protected lateinit var viewModel: VM

    private fun initViewModel(): VM =
        ViewModelProviders.of(this, viewModelFactory)
            .get(
                (this.javaClass
                    .genericSuperclass as ParameterizedType)
                    .actualTypeArguments[0] as Class<VM>
            )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = initViewModel()
        onViewModelCreated(viewModel)
        viewModel.start()
    }

    @CallSuper
    protected open fun onViewModelCreated(viewModel: VM) {
        viewModel.generalErrorLiveData.observe(
            this,
            Observer {
                it.getContentIfNotHandled()?.let { throwable ->
                    showSnackBarError(throwable)
                }
            }
        )
    }

    override fun onRetry() {
        super.onRetry()
    }

    protected fun showSnackBarError(throwable: Throwable) {
        val message = if (throwable is NoNetworkException) {
            getString(R.string.network_error)
        } else {
            throwable.message ?: getString(R.string.unexpected_error)
        }
        showSnackBarError(message)
    }

    protected fun showSnackBarError(message: String) {
        view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG).show()
        }
    }

    protected abstract inner class LiveDataObserver<T> : Observer<RxResult<T>> {

        override fun onChanged(t: RxResult<T>?) {
            when (t) {
                is RxResult.Success -> {
                    handleSuccessResult(t.data)
                    hideLoader(false)
                }
                is RxResult.Error -> {
                    t.exception.printStackTrace()
                    showError(t.exception)
                    hideLoader(true)
                }
                is RxResult.Loading -> {
                    showLoader()
                }
            }
        }

        open fun showLoader() {
        }

        open fun hideLoader(error: Boolean) {

        }

        open fun showError(throwable: Throwable) {
        }

        abstract fun handleSuccessResult(result: T)

    }

    protected abstract inner class SimpleLiveDataObserver<T> : LiveDataObserver<T>() {

        override fun showError(throwable: Throwable) {
            showSnackBarError(throwable)
        }

    }

    protected abstract inner class FullScreenLiveDataObserver<T> : LiveDataObserver<T>() {

        override fun showLoader() {
            showFullScreenLoaderFragment()
        }

        override fun hideLoader(error: Boolean) {
            hideFullScreenLoaderFragment()
        }

        override fun showError(throwable: Throwable) {
            var errorType = ErrorFragment.ErrorType.OTHER
            if (throwable is NoNetworkException)
                errorType = ErrorFragment.ErrorType.NETWORK
            showFullScreenError(ErrorFragment.ErrorModel(errorType))
        }

    }


}