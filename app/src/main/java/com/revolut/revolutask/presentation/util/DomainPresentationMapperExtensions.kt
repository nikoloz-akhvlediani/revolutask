package com.revolut.revolutask.presentation.util

import com.revolut.revolutask.domain.model.CurrencyListDataModel
import com.revolut.revolutask.presentation.screens.home.CurrencyRate

fun CurrencyListDataModel.mapToDomain() : List<CurrencyRate> {
        val list = mutableListOf<CurrencyRate>()
        list.add(
            CurrencyRate(
                base,
                1.0
            )
        )
        list.addAll(
            rates.map {
                CurrencyRate(
                    it.key,
                    it.value
                )
            }
        )
        return list
}