package com.revolut.revolutask.presentation

import com.revolut.revolutask.presentation.di.AppComponent
import com.revolut.revolutask.presentation.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class RevolutaskApp : DaggerApplication() {

    private val component: AppComponent by lazy {
        DaggerAppComponent.builder().application(this).build()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = component

}