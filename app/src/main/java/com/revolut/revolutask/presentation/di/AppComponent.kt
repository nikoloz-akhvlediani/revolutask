package com.revolut.revolutask.presentation.di

import com.revolut.revolutask.common.di.SchedulersModule
import com.revolut.revolutask.data.di.ApiModule
import com.revolut.revolutask.data.di.DataSourceModule
import com.revolut.revolutask.data.di.NetworkModule
import com.revolut.revolutask.data.di.RepositoryModule
import com.revolut.revolutask.domain.di.UseCaseModule
import com.revolut.revolutask.presentation.RevolutaskApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        ActivitySubComponentModule::class,
        FragmentSubComponentModule::class,
        ViewModelModule::class,
        ViewModelFactoryModule::class,
        AndroidInjectionModule::class,
        SchedulersModule::class,
        UseCaseModule::class,
        RepositoryModule::class,
        DataSourceModule::class,
        NetworkModule::class,
        ApiModule::class
    ]
)

interface AppComponent : AndroidInjector<RevolutaskApp> {

    override fun inject(instance: RevolutaskApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(revolutaskApp: RevolutaskApp): Builder

        fun build(): AppComponent

    }
}
