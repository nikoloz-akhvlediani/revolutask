package com.revolut.revolutask.presentation.di

import androidx.lifecycle.ViewModel
import com.revolut.revolutask.presentation.screens.home.CurrencyRatesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyRatesViewModel::class)
    abstract fun bindLoginViewModel(viewModel: CurrencyRatesViewModel): ViewModel

}