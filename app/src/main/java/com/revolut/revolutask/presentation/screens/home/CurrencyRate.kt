package com.revolut.revolutask.presentation.screens.home

data class CurrencyRate(
    val currency: String,
    var rate: Double
)