package com.revolut.revolutask.presentation.base.fragment

import com.revolut.revolutask.R


class LoaderFragment : BaseFragment() {

    override val layoutRes: Int
        get() = R.layout.fragment_loader

    companion object {

        fun createInstance(): LoaderFragment {
            return LoaderFragment()
        }

    }

}
