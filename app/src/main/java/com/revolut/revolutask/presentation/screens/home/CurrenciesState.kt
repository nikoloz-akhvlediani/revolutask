package com.revolut.revolutask.presentation.screens.home

class CurrenciesState(
    private var selectedCurrency: CurrencyRate,
    val currencyList: MutableList<CurrencyRate>
) {

    var enteredValue = 1.0

    var enteredBaseValue = 1.0
        get() = enteredValue / selectedCurrency.rate

    fun selectCurrency(currencyRate: CurrencyRate): Int {
        enteredValue = enteredBaseValue * currencyRate.rate
        val index = currencyList.indexOf(currencyRate)
        currencyList.remove(currencyRate)
        currencyList.add(0, currencyRate)
        selectedCurrency = currencyRate
        return index
    }

    fun updateData(rates: Map<String, Double>) {
        currencyList.forEach { currencyRate ->
            val value = rates[currencyRate.currency]
            value?.let {
                if (currencyRate.currency == selectedCurrency.currency)
                    selectedCurrency.rate = it
                currencyRate.rate = it
            }
        }
    }

}