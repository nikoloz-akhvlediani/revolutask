package com.revolut.revolutask.presentation.mvvm.model

sealed class RxResult<out T> {

    data class Success<out T>(val data: T) : RxResult<T>()
    data class Error(val exception: Throwable) : RxResult<Nothing>()
    object Loading : RxResult<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            is Loading -> "Loading="
        }
    }

    fun getResultIfSuccess(): T? =
        if (this is Success) data else null
}

val RxResult<*>.succeeded
    get() = this is RxResult.Success && data != null