package com.revolut.revolutask.presentation.mvvm.viewmodel

import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.revolut.revolutask.common.model.RxEvent
import com.revolut.revolutask.common.rx.BasicSchedulersObservableTransformer
import com.revolut.revolutask.domain.usecase.base.BaseUseCase
import com.revolut.revolutask.domain.usecase.base.UseCaseExecutor
import com.revolut.revolutask.presentation.mvvm.model.RxResult
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

typealias ResultLiveData<T> = LiveData<RxResult<T>>
typealias ResultMutableLiveData<T> = MutableLiveData<RxResult<T>>

typealias EventResultLiveData<T> = LiveData<RxResult<RxEvent<T>>>
typealias EventResultMutableLiveData<T> = MutableLiveData<RxResult<RxEvent<T>>>

typealias EventLiveData<T> = LiveData<RxEvent<T>>
typealias EventMutableLiveData<T> = MutableLiveData<RxEvent<T>>

open class BaseViewModel(
    protected val useCaseExecutor: UseCaseExecutor
) : ViewModel() {

    private val _generalErrorLiveData = EventMutableLiveData<Throwable>()
    val generalErrorLiveData: EventLiveData<Throwable>
        get() = _generalErrorLiveData

    private val disposables = CompositeDisposable()

    protected fun Disposable?.add() {
        this?.let(disposables::add)
    }

    @CallSuper
    open fun start() {
        setUpValidation()
    }

    @CallSuper
    protected open fun setUpValidation() {

    }

    protected fun setError(throwable: Throwable) {
        _generalErrorLiveData.postValue(RxEvent(throwable))
    }

    override fun onCleared() {
        disposables.clear()
        useCaseExecutor.clear()
        super.onCleared()
    }

    protected fun <R> subscribeToUseCase(
        useCase: BaseUseCase<*, R>,
        successBlock: (r: R) -> Unit = {},
        errorBlock: (t: Throwable) -> Unit = {}
    ) {
        useCase.successObservable
            .compose(BasicSchedulersObservableTransformer())
            .subscribe({
                successBlock(it)
            }, {
                errorBlock(it)
            })
            .add()
        useCase.errorObservable
            .compose(BasicSchedulersObservableTransformer())
            .subscribe({
                errorBlock(it)
            }, {
                errorBlock(it)
            })
            .add()
    }

}