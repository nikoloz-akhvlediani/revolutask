package com.revolut.revolutask.presentation.screens.home

import com.revolut.revolutask.common.model.RxEvent
import com.revolut.revolutask.domain.usecase.GetCurrencyRatesUseCase
import com.revolut.revolutask.domain.usecase.UpdateCurrencyRatesUseCase
import com.revolut.revolutask.domain.usecase.base.UseCaseExecutor
import com.revolut.revolutask.presentation.mvvm.model.RxResult
import com.revolut.revolutask.presentation.mvvm.viewmodel.*
import com.revolut.revolutask.presentation.util.mapToDomain
import javax.inject.Inject

class CurrencyRatesViewModel @Inject constructor(
    private val getCurrencyRatesUseCase: GetCurrencyRatesUseCase,
    private val updateCurrencyRatesUseCase: UpdateCurrencyRatesUseCase,
    useCaseExecutor: UseCaseExecutor
) : BaseViewModel(useCaseExecutor) {

    var baseCurrency = DEFAULT_BASE_CURRENCY

    private val _currencyRatesLiveData = EventResultMutableLiveData<List<CurrencyRate>>()
    val currencyRatesLiveData: EventResultLiveData<List<CurrencyRate>>
        get() = _currencyRatesLiveData

    private val _updatedCurrencyRatesLiveData =
        EventResultMutableLiveData<UpdateCurrencyDataModel>()
    val updatedCurrencyRatesLiveData: EventResultLiveData<UpdateCurrencyDataModel>
        get() = _updatedCurrencyRatesLiveData

    private val _selectCurrencyLiveData = EventMutableLiveData<Int>()
    val selectCurrencyLiveData: EventLiveData<Int>
        get() = _selectCurrencyLiveData

    private lateinit var currenciesState: CurrenciesState


    override fun start() {
        super.start()
        subscribeToUseCase(
            getCurrencyRatesUseCase,
            {
                currenciesState = CurrenciesState(
                    CurrencyRate(it.base, 1.0),
                    it.mapToDomain().toMutableList()
                )
                _currencyRatesLiveData.postValue(RxResult.Success(RxEvent(currenciesState.currencyList)))
                useCaseExecutor.executeUseCase(updateCurrencyRatesUseCase, baseCurrency)
            }, {
                _currencyRatesLiveData.postValue(RxResult.Error(it))
            }
        )
        subscribeToUseCase(
            updateCurrencyRatesUseCase,
            {
                currenciesState.updateData(it.rates)
                notifyUpdatedData()
            },
            {
                _updatedCurrencyRatesLiveData.postValue(RxResult.Error(it))
            }
        )
        getCurrencyRates()
    }

    fun getCurrencyRates() {
        _currencyRatesLiveData.postValue(RxResult.Loading)
        useCaseExecutor.executeUseCase(getCurrencyRatesUseCase, baseCurrency)
    }

    fun updateEnteredValue(enteredValue: Double) {
        currenciesState.enteredValue = enteredValue
        notifyUpdatedData()
    }

    fun selectCurrency(currencyRate: CurrencyRate) {
        val index = currenciesState.selectCurrency(currencyRate)
        _selectCurrencyLiveData.postValue(RxEvent(index))
    }

    private fun notifyUpdatedData() {
        _updatedCurrencyRatesLiveData.postValue(
            RxResult.Success(
                RxEvent(
                    UpdateCurrencyDataModel(
                        currenciesState.currencyList,
                        currenciesState.enteredBaseValue
                    )
                )
            )
        )
    }

    companion object {

        private const val DEFAULT_BASE_CURRENCY = "EUR"

    }

}