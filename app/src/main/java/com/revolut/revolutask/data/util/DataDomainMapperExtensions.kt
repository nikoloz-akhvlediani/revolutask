package com.revolut.revolutask.data.util

import android.annotation.SuppressLint
import com.revolut.revolutask.data.remote.model.LatestCurrenciesRemoteModel
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import java.text.SimpleDateFormat

@SuppressLint("SimpleDateFormat")
fun String.mapDateStringToDomain(): Long {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    val dateObj = dateFormat.parse(this)
    return dateObj?.time ?: 0
}

fun LatestCurrenciesRemoteModel.mapToDomain() =
    CurrencyListDataModel(base, date.mapDateStringToDomain(), rates)