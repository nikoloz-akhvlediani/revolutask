package com.revolut.revolutask.data.di

import com.revolut.revolutask.data.datasource.remote.CurrencyRemoteDataSource
import com.revolut.revolutask.data.remote.datasource.CurrencyRemoteDataSourceImpl
import dagger.Module
import dagger.Provides

@Module
class DataSourceModule {

    @Provides
    fun provideCurrencyRemoteDataSource(currencyRemoteDataSourceImpl: CurrencyRemoteDataSourceImpl)
            : CurrencyRemoteDataSource = currencyRemoteDataSourceImpl

}