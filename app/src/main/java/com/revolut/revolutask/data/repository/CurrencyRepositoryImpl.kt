package com.revolut.revolutask.data.repository

import com.revolut.revolutask.data.datasource.remote.CurrencyRemoteDataSource
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import com.revolut.revolutask.domain.repository.CurrencyRepository
import io.reactivex.Single
import javax.inject.Inject

class CurrencyRepositoryImpl @Inject constructor(
    private val currencyRemoteDataSource: CurrencyRemoteDataSource
) : CurrencyRepository {

    override fun getLatestCurrencies(base: String): Single<CurrencyListDataModel> =
        currencyRemoteDataSource.getLatestCurrencies(base)

}