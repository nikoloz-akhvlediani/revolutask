package com.revolut.revolutask.data.remote.datasource

import com.revolut.revolutask.data.datasource.remote.CurrencyRemoteDataSource
import com.revolut.revolutask.data.remote.CurrencyApi
import com.revolut.revolutask.data.util.mapToDomain
import com.revolut.revolutask.data.util.rx.transformer.ApiErrorOperator
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import io.reactivex.Single
import javax.inject.Inject

class CurrencyRemoteDataSourceImpl @Inject constructor(
    private val currencyApi: CurrencyApi
) : CurrencyRemoteDataSource {

    override fun getLatestCurrencies(base: String): Single<CurrencyListDataModel> {
        return currencyApi.getLatestCurrencies(base)
            .lift(ApiErrorOperator())
            .map {
                it.mapToDomain()
            }
    }

}