package com.revolut.revolutask.data.datasource.remote

import com.revolut.revolutask.domain.model.CurrencyListDataModel
import io.reactivex.Single

interface CurrencyRemoteDataSource {

    fun getLatestCurrencies(base: String): Single<CurrencyListDataModel>

}