package com.revolut.revolutask.data.exception

class NoNetworkException : Exception()
