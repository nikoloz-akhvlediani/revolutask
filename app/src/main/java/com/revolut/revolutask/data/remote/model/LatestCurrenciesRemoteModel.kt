package com.revolut.revolutask.data.remote.model

import com.google.gson.annotations.SerializedName

data class LatestCurrenciesRemoteModel(
    @SerializedName("base")
    val base: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("rates")
    val rates: Map<String, Double>
)