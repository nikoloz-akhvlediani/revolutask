package com.revolut.revolutask.data.util.rx.transformer

import com.revolut.revolutask.data.exception.ApiException
import com.revolut.revolutask.data.exception.NoNetworkException
import io.reactivex.SingleObserver
import io.reactivex.SingleOperator
import io.reactivex.exceptions.Exceptions
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.plugins.RxJavaPlugins
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


class ApiErrorOperator<T> : SingleOperator<T, Response<T>> {

    @Throws(Exception::class)
    override fun apply(observer: SingleObserver<in T>): SingleObserver<in Response<T>> {
        return ResponseMappingObserver(observer)
    }

    private class ResponseMappingObserver<T>(
        private val downstream: SingleObserver<in T>
    ) : DisposableSingleObserver<Response<T>>() {

        override fun onStart() {
            downstream.onSubscribe(this)
        }

        override fun onSuccess(response: Response<T>) {
            try {
                if (!response.isSuccessful) {
                    throw ApiException.RequestFailedException(response.code())
                } else {
                    val message =
                        response.body() ?: throw ApiException.InvalidResponseException(response)
                    if (!isDisposed) {
                        downstream.onSuccess(message)
                    }
                }
            } catch (e: Exception) {
                Exceptions.throwIfFatal(e)
                onError(e)
            }
        }

        override fun onError(e: Throwable) {
            if (isDisposed) {
                RxJavaPlugins.onError(e)
            } else {
                if (e is SocketException
                    || e is UnknownHostException
                    || e is SocketTimeoutException
                ) {
                    downstream.onError(NoNetworkException())
                } else {
                    downstream.onError(e)
                }
            }
        }
    }

}