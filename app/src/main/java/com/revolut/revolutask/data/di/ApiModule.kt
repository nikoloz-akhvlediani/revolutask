package com.revolut.revolutask.data.di

import com.revolut.revolutask.BuildConfig
import com.revolut.revolutask.data.remote.CurrencyApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class ApiModule {

    @Provides
    fun provideCurrencyApi(
        retrofitBuilder: Retrofit.Builder,
        @Named(CURRENCY_API_BASE_URL) baseUrl: String
    ): CurrencyApi {
        val retrofit = retrofitBuilder.baseUrl(baseUrl).build()
        return retrofit.create(CurrencyApi::class.java)
    }

    @Provides
    @Named(CURRENCY_API_BASE_URL)
    fun provideCurrencyApiBaseUrl() = BuildConfig.BASE_URL

    companion object {

        private const val CURRENCY_API_BASE_URL = "currencyApiBaseUrl"

    }

}