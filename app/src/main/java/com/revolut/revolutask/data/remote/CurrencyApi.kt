package com.revolut.revolutask.data.remote

import com.revolut.revolutask.data.remote.model.LatestCurrenciesRemoteModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET(LATEST)
    fun getLatestCurrencies(@Query(BASE) base: String): Single<Response<LatestCurrenciesRemoteModel>>

    companion object {

        private const val LATEST = "/latest"
        private const val BASE = "base"

    }

}