package com.revolut.revolutask.data.di

import com.revolut.revolutask.data.repository.CurrencyRepositoryImpl
import com.revolut.revolutask.domain.repository.CurrencyRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun provideCurrencyRepository(currencyRepositoryImpl: CurrencyRepositoryImpl): CurrencyRepository =
        currencyRepositoryImpl

}