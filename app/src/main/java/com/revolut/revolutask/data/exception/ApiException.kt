package com.revolut.revolutask.data.exception

import retrofit2.Response

sealed class ApiException(
    message: String? = null
) : Exception(message) {

    class RequestFailedException(val code: Int) : ApiException()

    class InvalidResponseException(val response: Response<*>) : ApiException()

    class InternalErrorException(
        val errorCode: Int,
        errorDescription: String? = null
    ) : ApiException(errorDescription)


}