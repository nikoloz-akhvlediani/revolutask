package com.revolut.revolutask.domain.di

import com.revolut.revolutask.common.rx.BaseSchedulerProvider
import com.revolut.revolutask.domain.repository.CurrencyRepository
import com.revolut.revolutask.domain.usecase.GetCurrencyRatesUseCase
import com.revolut.revolutask.domain.usecase.UpdateCurrencyRatesUseCase
import com.revolut.revolutask.domain.usecase.base.UseCaseExecutor
import com.revolut.revolutask.domain.usecase.base.UseCaseExecutorImpl
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    @Provides
    fun provideUseCaseExecutor(useCaseExecutorImpl: UseCaseExecutorImpl): UseCaseExecutor =
        useCaseExecutorImpl

    @Provides
    fun provideGetCurrencyRatesUseCase(currencyRepository: CurrencyRepository): GetCurrencyRatesUseCase =
        GetCurrencyRatesUseCase(currencyRepository)

    @Provides
    fun provideUpdateCurrencyRatesUseCase(
        getCurrencyRatesUseCase: GetCurrencyRatesUseCase,
        baseSchedulerProvider: BaseSchedulerProvider
    ): UpdateCurrencyRatesUseCase = UpdateCurrencyRatesUseCase(
        getCurrencyRatesUseCase,
        baseSchedulerProvider,
        baseSchedulerProvider
    )

}