package com.revolut.revolutask.domain.usecase

import com.revolut.revolutask.common.rx.BaseSchedulerProvider
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import com.revolut.revolutask.domain.usecase.base.BaseUseCase
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class UpdateCurrencyRatesUseCase(
    private val getCurrencyRatesUseCase: GetCurrencyRatesUseCase,
    private val schedulerProvider: BaseSchedulerProvider,
    private val timerSchedulerProvider: BaseSchedulerProvider
) : BaseUseCase<String, CurrencyListDataModel>() {

    private var disposable: Disposable? = null

    init {
        errorSubject.onNext(Exception())
        getCurrencyRatesUseCase.successObservable
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe({
                successSubject.onNext(it)
            }, {
                errorSubject.onNext(it)
            })
            .add()
        getCurrencyRatesUseCase.errorObservable
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe({
                errorSubject.onNext(it)
            }, {
                errorSubject.onNext(it)
            })
            .add()
    }

    override fun execute(params: String) {
        disposable?.dispose()
        disposable = Observable
            .interval(1L, TimeUnit.SECONDS, timerSchedulerProvider.computation())
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe({
                getCurrencyRatesUseCase.execute(params)
            }, {

            })
        disposable.add()
    }


}