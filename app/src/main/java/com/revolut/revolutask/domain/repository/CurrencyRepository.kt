package com.revolut.revolutask.domain.repository

import com.revolut.revolutask.domain.model.CurrencyListDataModel
import io.reactivex.Single

interface CurrencyRepository {

    fun getLatestCurrencies(base: String): Single<CurrencyListDataModel>

}