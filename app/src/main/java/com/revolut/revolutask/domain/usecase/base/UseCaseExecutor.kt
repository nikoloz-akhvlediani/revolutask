package com.revolut.revolutask.domain.usecase.base

interface UseCaseExecutor {

    fun<T> executeUseCase(useCase: UseCase<T>, params: T)

    fun cancelUseCase(useCase: UseCase<*>)

    fun clear()

}