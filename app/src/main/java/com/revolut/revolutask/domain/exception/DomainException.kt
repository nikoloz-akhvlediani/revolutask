package com.revolut.revolutask.domain.exception

open class DomainException(
    message: String? = null
) : Exception(message)