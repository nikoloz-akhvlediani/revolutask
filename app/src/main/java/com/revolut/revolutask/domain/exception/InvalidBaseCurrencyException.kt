package com.revolut.revolutask.domain.exception

class InvalidBaseCurrencyException : DomainException()