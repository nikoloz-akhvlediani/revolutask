package com.revolut.revolutask.domain.usecase.base

import androidx.annotation.VisibleForTesting
import java.util.*
import javax.inject.Inject

class UseCaseExecutorImpl @Inject constructor() :
    UseCaseExecutor {

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    val stack = Stack<UseCase<*>>()

    override fun <T> executeUseCase(useCase: UseCase<T>, params: T) {
        useCase.execute(params)
        if (stack.contains(useCase))
            stack.remove(useCase)
        stack.push(useCase)
    }

    override fun cancelUseCase(useCase: UseCase<*>) {
        useCase.cancel()
    }

    override fun clear() {
        stack.forEach {
            it.clear()
        }
        stack.clear()
    }

}