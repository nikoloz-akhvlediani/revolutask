package com.revolut.revolutask.domain.usecase.base

import com.revolut.revolutask.data.exception.NoNetworkException
import com.revolut.revolutask.domain.exception.DomainException
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subjects.PublishSubject

abstract class BaseUseCase<Input, OutPut> : UseCase<Input> {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    protected val successSubject = PublishSubject.create<OutPut>()
    val successObservable: Observable<OutPut>
        get() = successSubject

    protected val errorSubject = PublishSubject.create<Throwable>()
    val errorObservable: Observable<Throwable>
        get() = errorSubject

    override fun cancel() {
        clear()
    }

    override fun clear() {
        compositeDisposable.dispose()
    }

    protected fun Disposable?.add() {
        this?.let(compositeDisposable::add)
    }

    protected inner class UseCaseSingleOperator : SingleOperator<OutPut, OutPut> {

        override fun apply(observer: SingleObserver<in OutPut>): SingleObserver<in OutPut> {
            return ResponseMappingSingle()
        }

        inner class ResponseMappingSingle : DisposableSingleObserver<OutPut>() {

            override fun onSuccess(t: OutPut) {
                successSubject.onNext(t)
            }

            override fun onError(e: Throwable) {
                errorSubject.onNext(e)
            }

        }

    }

    protected inner class UseCaseObservableOperator : ObservableOperator<OutPut, OutPut> {

        override fun apply(observer: Observer<in OutPut>): Observer<in OutPut> {
            return ResponseMappingObserver()
        }


        inner class ResponseMappingObserver : DisposableObserver<OutPut>() {

            override fun onComplete() {
                successSubject.onComplete()
            }

            override fun onNext(t: OutPut) {
                successSubject.onNext(t)
            }

            override fun onError(e: Throwable) {
                errorSubject.onNext(e)
            }


        }

    }

    protected inner class MapErrors<T> : Function<Throwable, SingleSource<T>> {

        override fun apply(it: Throwable): SingleSource<T> {
            var exception = it
            if (it !is NoNetworkException && it !is DomainException) {
                exception = DomainException()
            }
            return Single.error(exception)
        }

    }

}