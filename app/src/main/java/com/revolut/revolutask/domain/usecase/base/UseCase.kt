package com.revolut.revolutask.domain.usecase.base

interface UseCase<T> {

    fun execute(params: T)

    fun cancel()

    fun clear()

}