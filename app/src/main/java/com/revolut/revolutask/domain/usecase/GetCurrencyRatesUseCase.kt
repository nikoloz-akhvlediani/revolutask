package com.revolut.revolutask.domain.usecase

import com.revolut.revolutask.common.rx.BasicSchedulersSingleTransformer
import com.revolut.revolutask.domain.exception.InvalidBaseCurrencyException
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import com.revolut.revolutask.domain.repository.CurrencyRepository
import com.revolut.revolutask.domain.usecase.base.BaseUseCase

class GetCurrencyRatesUseCase(
    private val currencyRepository: CurrencyRepository
) : BaseUseCase<String, CurrencyListDataModel>() {

    override fun execute(params: String) {
        currencyRepository.getLatestCurrencies(params)
            .compose(BasicSchedulersSingleTransformer())
            .doOnSuccess {
                if (it.base != params)
                    throw InvalidBaseCurrencyException()
            }
            .onErrorResumeNext(MapErrors())
            .lift(UseCaseSingleOperator())
            .subscribe()
            .add()
    }

}