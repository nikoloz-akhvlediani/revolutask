package com.revolut.revolutask.domain.model

data class CurrencyListDataModel(
    val base: String,
    val date: Long,
    val rates: Map<String, Double>
)