package com.revolut.revolutask.presentation.screens.home

import com.revolut.revolutask.domain.model.CurrencyListDataModel
import com.revolut.revolutask.domain.usecase.GetCurrencyRatesUseCase
import com.revolut.revolutask.domain.usecase.UpdateCurrencyRatesUseCase
import com.revolut.revolutask.domain.usecase.base.UseCaseExecutor
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.runner.screenshot.Screenshot.capture
import com.revolut.revolutask.common.model.RxEvent
import com.revolut.revolutask.domain.exception.DomainException
import com.revolut.revolutask.domain.usecase.base.UseCase
import com.revolut.revolutask.presentation.mvvm.model.RxResult
import com.revolut.revolutask.presentation.util.mapToDomain
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import org.junit.rules.TestRule
import org.junit.Rule
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.mock
import org.mockito.Mockito.times


@RunWith(MockitoJUnitRunner::class)
class CurrencyRatesViewModelTest {

    @Mock
    private lateinit var getCurrencyRatesUseCase: GetCurrencyRatesUseCase
    @Mock
    private lateinit var updateCurrencyRatesUseCase: UpdateCurrencyRatesUseCase

    private lateinit var viewModel: CurrencyRatesViewModel

    private val useCaseExecutor = UseCaseExecutorMock()

    private lateinit var currencyListDataModel1: CurrencyListDataModel
    private lateinit var currencyListDataModel2: CurrencyListDataModel

    private lateinit var getDataSuccessSubject: PublishSubject<CurrencyListDataModel>
    private lateinit var getDataErrorSubject: PublishSubject<Throwable>
    private lateinit var updateDataSuccessSubject: PublishSubject<CurrencyListDataModel>
    private lateinit var updateDataErrorSubject: PublishSubject<Throwable>

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var getDataObserver: Observer<RxResult<RxEvent<List<CurrencyRate>>>>
    @Mock
    private lateinit var updateDataObserver: Observer<RxResult<RxEvent<UpdateCurrencyDataModel>>>
    @Mock
    private lateinit var selectedCurrencyObserver: Observer<RxEvent<Int>>

    @Before
    fun setUp() {
        getDataSuccessSubject = PublishSubject.create()
        getDataErrorSubject = PublishSubject.create()
        updateDataSuccessSubject = PublishSubject.create()
        updateDataErrorSubject = PublishSubject.create()
        Mockito.`when`(getCurrencyRatesUseCase.successObservable)
            .thenReturn(getDataSuccessSubject)
        Mockito.`when`(getCurrencyRatesUseCase.errorObservable)
            .thenReturn(getDataErrorSubject)
        Mockito.`when`(updateCurrencyRatesUseCase.successObservable)
            .thenReturn(updateDataSuccessSubject)
        Mockito.`when`(updateCurrencyRatesUseCase.errorObservable)
            .thenReturn(updateDataErrorSubject)

        viewModel = CurrencyRatesViewModel(
            getCurrencyRatesUseCase,
            updateCurrencyRatesUseCase,
            useCaseExecutor
        )
        currencyListDataModel1 = CurrencyListDataModel(
            "EUR",
            1574501949484L,
            mapOf(
                "AUD" to 1.6097,
                "BGN" to 1.9477,
                "BRL" to 4.7719,
                "CAD" to 1.5274
            )
        )
        currencyListDataModel2 = CurrencyListDataModel(
            "EUR",
            1574501949484L,
            mapOf(
                "AUD" to 1.6297,
                "BGN" to 1.9577,
                "BRL" to 4.7919,
                "CAD" to 1.5574
            )
        )
        viewModel.baseCurrency = BASE_CURRENCY
        viewModel.currencyRatesLiveData.observeForever(getDataObserver)
        viewModel.updatedCurrencyRatesLiveData.observeForever(updateDataObserver)
        viewModel.selectCurrencyLiveData.observeForever(selectedCurrencyObserver)
    }

    @Test
    fun start_verifyThatGetDataUseCaseIsExecuted() {
        viewModel.start()
        assertEquals(useCaseExecutor.counter, 1)
    }

    @Test
    fun start_checkData() {
        Mockito.`when`(getCurrencyRatesUseCase.execute(BASE_CURRENCY))
            .then {
                getDataSuccessSubject.onNext(currencyListDataModel1)
            }
        viewModel.start()
        val captor = ArgumentCaptor.forClass(RxResult::class.java)
        captor.run {
            Mockito.verify(getDataObserver, times(2))
                .onChanged(capture() as RxResult<RxEvent<List<CurrencyRate>>>?)
            val result = value.getResultIfSuccess() as RxEvent<List<CurrencyRate>>?
            assertEquals(currencyListDataModel1.mapToDomain(), result?.peekContent())
        }
    }

    @Test
    fun start_checkErrors() {
        Mockito.`when`(getCurrencyRatesUseCase.execute(BASE_CURRENCY))
            .then {
                getDataErrorSubject.onNext(DomainException())
            }
        viewModel.start()
        val captor = ArgumentCaptor.forClass(RxResult::class.java)
        captor.run {
            // Loading and actual data
            Mockito.verify(getDataObserver, times(2))
                .onChanged(capture() as RxResult<RxEvent<List<CurrencyRate>>>?)
            val result = value as RxResult.Error
            assertTrue(result.exception is DomainException)
        }
    }

    @Test
    fun start_verifyThatUpdateDataUseCaseIsExecuted() {
        Mockito.`when`(getCurrencyRatesUseCase.execute(BASE_CURRENCY))
            .then {
                getDataSuccessSubject.onNext(currencyListDataModel1)
            }
        viewModel.start()
        Mockito.verify(updateCurrencyRatesUseCase, times(1)).execute(BASE_CURRENCY)
    }

    @Test
    fun start_checkUpdatedData() {
        Mockito.`when`(getCurrencyRatesUseCase.execute(BASE_CURRENCY))
            .then {
                getDataSuccessSubject.onNext(currencyListDataModel1)
            }
        Mockito.`when`(updateCurrencyRatesUseCase.execute(BASE_CURRENCY))
            .then {
                updateDataSuccessSubject.onNext(currencyListDataModel2)
            }
        viewModel.start()
        val captor = ArgumentCaptor.forClass(RxResult::class.java)
        captor.run {
            Mockito.verify(updateDataObserver, times(1))
                .onChanged(capture() as RxResult<RxEvent<UpdateCurrencyDataModel>>?)
            val result = value.getResultIfSuccess() as RxEvent<UpdateCurrencyDataModel>?
            assertEquals(
                UpdateCurrencyDataModel(currencyListDataModel2.mapToDomain(), 1.0),
                result?.peekContent()
            )
        }
    }

    @Test
    fun start_updateEnteredValue() {
        Mockito.`when`(getCurrencyRatesUseCase.execute(BASE_CURRENCY))
            .then {
                getDataSuccessSubject.onNext(currencyListDataModel1)
            }
        viewModel.start()
        viewModel.updateEnteredValue(2.0)
        val captor = ArgumentCaptor.forClass(RxResult::class.java)
        captor.run {
            Mockito.verify(updateDataObserver, times(1))
                .onChanged(capture() as RxResult<RxEvent<UpdateCurrencyDataModel>>?)
            val result = value.getResultIfSuccess() as RxEvent<UpdateCurrencyDataModel>?
            assertEquals(
                UpdateCurrencyDataModel(currencyListDataModel1.mapToDomain(), 2.0),
                result?.peekContent()
            )
        }
    }

    @Test
    fun start_selectCurrency() {
        Mockito.`when`(getCurrencyRatesUseCase.execute(BASE_CURRENCY))
            .then {
                getDataSuccessSubject.onNext(currencyListDataModel1)
            }
        val currency = currencyListDataModel1.mapToDomain().get(2)
        viewModel.start()
        viewModel.selectCurrency(currency)
        val captor = ArgumentCaptor.forClass(RxEvent::class.java)
        captor.run {
            Mockito.verify(selectedCurrencyObserver, times(1))
                .onChanged(capture() as RxEvent<Int>?)
            assertEquals(
                2, value?.peekContent()
            )
        }
    }

    companion object {

        private const val BASE_CURRENCY = "EUR"

        @BeforeClass
        @JvmStatic
        fun before() {
            RxAndroidPlugins.reset()
            RxJavaPlugins.reset()
            RxJavaPlugins.setIoSchedulerHandler {
                return@setIoSchedulerHandler Schedulers.trampoline()
            }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler {
                return@setInitMainThreadSchedulerHandler Schedulers.trampoline()
            }
        }

        @AfterClass
        @JvmStatic
        fun after() {
            RxAndroidPlugins.reset()
            RxJavaPlugins.reset()
        }
    }

    private class UseCaseExecutorMock : UseCaseExecutor {

        var counter = 0

        override fun <T> executeUseCase(useCase: UseCase<T>, params: T) {
            counter++
            useCase.execute(params)
        }

        override fun cancelUseCase(useCase: UseCase<*>) {
            useCase.cancel()
        }

        override fun clear() {
        }


    }

}