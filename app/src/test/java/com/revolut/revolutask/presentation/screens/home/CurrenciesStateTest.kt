package com.revolut.revolutask.presentation.screens.home

import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CurrenciesStateTest {

    private lateinit var currenciesState: CurrenciesState

    private val eur = CurrencyRate("EUR", 1.0)
    private val usd = CurrencyRate("USD", 1.12)
    private val gbp = CurrencyRate("GBP", 0.6)
    private val aud = CurrencyRate("AUD", 1.6)
    private val cad = CurrencyRate("CAD", 1.5)

    val updatedData = mapOf(
        "EUR" to 1.0,
        "USD" to 1.14,
        "GBP" to 0.62,
        "AUD" to 1.6,
        "CAD" to 1.51
    )

    private val currencyList = listOf(eur, usd, gbp, aud, cad)

    @Before
    fun setUp() {
        currenciesState = CurrenciesState(
            eur,
            currencyList.toMutableList()
        )
    }

    @Test
    fun enteredValue() {
        with(currenciesState) {
            enteredValue = 1.0
            assertEquals(1.0, enteredValue)
            assertEquals(1.0, enteredBaseValue)

            enteredValue = 2.0
            assertEquals(2.0, enteredValue)
            assertEquals(2.0, enteredBaseValue)

            enteredValue = 0.6
            assertEquals(0.6, enteredValue)
            assertEquals(0.6, enteredBaseValue)

            enteredValue = 3.0
            assertEquals(3.0, enteredValue)
            assertEquals(3.0, enteredBaseValue)
        }
    }

    @Test
    fun selectCurrency() {
        with(currenciesState) {
            enteredValue = 3.0
            val index = selectCurrency(cad)

            assertEquals(4, index)
            assertEquals(4.5, enteredValue)
            assertEquals(3.0, enteredBaseValue)

            enteredValue = 3.0
            assertEquals(2.0, enteredBaseValue)

            val index2 = selectCurrency(eur)
            assertEquals(1, index2)
            assertEquals(2.0, enteredValue)
            assertEquals(2.0, enteredBaseValue)
        }
    }

    @Test
    fun updateData() {
        with(currenciesState) {
            enteredValue = 1.0
            selectCurrency(gbp)
            updateData(updatedData)
            currencyList.forEach {
                assertEquals(updatedData[it.currency], it.rate)
            }
            assertEquals(0.6 / 0.62, enteredBaseValue)
        }
    }

}