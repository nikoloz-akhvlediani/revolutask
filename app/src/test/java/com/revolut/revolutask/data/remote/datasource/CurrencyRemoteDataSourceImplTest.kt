package com.revolut.revolutask.data.remote.datasource

import com.revolut.revolutask.data.exception.ApiException
import com.revolut.revolutask.data.exception.NoNetworkException
import com.revolut.revolutask.data.remote.CurrencyApi
import com.revolut.revolutask.data.remote.model.LatestCurrenciesRemoteModel
import com.revolut.revolutask.data.util.mapDateStringToDomain
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import io.reactivex.Single
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


@RunWith(MockitoJUnitRunner::class)
class CurrencyRemoteDataSourceImplTest {

    @Mock
    private lateinit var currencyApi: CurrencyApi

    private lateinit var currencyRemoteDataSourceImpl: CurrencyRemoteDataSourceImpl

    private lateinit var remoteData1: LatestCurrenciesRemoteModel
    private lateinit var remoteMap1: Map<String, Double>

    private lateinit var remoteData2: LatestCurrenciesRemoteModel
    private lateinit var remoteMap2: Map<String, Double>


    @Before
    fun setUp() {
        currencyRemoteDataSourceImpl = CurrencyRemoteDataSourceImpl(currencyApi)

        remoteMap1 = mapOf(
            "AUD" to 1.6097,
            "BGN" to 1.9477,
            "BRL" to 4.7719,
            "CAD" to 1.5274
        )
        remoteData1 = LatestCurrenciesRemoteModel("EUR", "2018-09-06", remoteMap1)

        remoteMap2 = mapOf(
            "EUR" to 1.6097
        )
        remoteData2 = LatestCurrenciesRemoteModel("USD", "2018-09-06", remoteMap2)
    }

    @Test
    fun testSuccessfulCases() {
        Mockito.`when`(currencyApi.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.just(
                    Response.success(remoteData1)
                )
            )
        val domainData1 = CurrencyListDataModel(
            "EUR", "2018-09-06".mapDateStringToDomain(), mapOf(
                "AUD" to 1.6097,
                "BGN" to 1.9477,
                "BRL" to 4.7719,
                "CAD" to 1.5274
            )
        )
        currencyRemoteDataSourceImpl.getLatestCurrencies("EUR")
            .test()
            .assertResult(
                domainData1
            )

        Mockito.`when`(currencyApi.getLatestCurrencies("USD"))
            .thenReturn(
                Single.just(
                    Response.success(remoteData2)
                )
            )
        val domainData2 =
            CurrencyListDataModel(remoteData2.base, remoteData2.date.mapDateStringToDomain(), remoteData2.rates)
        currencyRemoteDataSourceImpl.getLatestCurrencies("USD")
            .test()
            .assertResult(
                domainData2
            )
    }

    @Test
    fun errorCases() {
        // Should throw RequestFailedException when there is an http error
        Mockito.`when`(currencyApi.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.just(
                    Response.error<LatestCurrenciesRemoteModel>(500, ResponseBody.create(null, ""))
                )
            )
        currencyRemoteDataSourceImpl.getLatestCurrencies("EUR")
            .test()
            .assertError(
                ApiException.RequestFailedException::class.java
            )

        // Should throw InvalidResponseException when response is null
        Mockito.`when`(currencyApi.getLatestCurrencies("EUR"))
             .thenReturn(
                 Single.just(
                     Response.success<LatestCurrenciesRemoteModel>(null)
                 )
             )
         currencyRemoteDataSourceImpl.getLatestCurrencies("EUR")
             .test()
             .assertError(
                 ApiException.InvalidResponseException::class.java
             )

        // Should throw NoNetwork exception when there is corresponding exceptions
        Mockito.`when`(currencyApi.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(SocketException())
            )
        currencyRemoteDataSourceImpl.getLatestCurrencies("EUR")
            .test()
            .assertError(
                NoNetworkException::class.java
            )
        Mockito.`when`(currencyApi.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(UnknownHostException())
            )
        currencyRemoteDataSourceImpl.getLatestCurrencies("EUR")
            .test()
            .assertError(
                NoNetworkException::class.java
            )
        Mockito.`when`(currencyApi.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(SocketTimeoutException())
            )
        currencyRemoteDataSourceImpl.getLatestCurrencies("EUR")
            .test()
            .assertError(
                NoNetworkException::class.java
            )

        // If exception is unknown should throw it
        Mockito.`when`(currencyApi.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(TestException())
            )
        currencyRemoteDataSourceImpl.getLatestCurrencies("EUR")
            .test()
            .assertError(
                TestException::class.java
            )
    }

    private class TestException : Exception()

}