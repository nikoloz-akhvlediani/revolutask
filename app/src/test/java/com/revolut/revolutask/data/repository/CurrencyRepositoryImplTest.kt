package com.revolut.revolutask.data.repository

import com.revolut.revolutask.data.datasource.remote.CurrencyRemoteDataSource
import com.revolut.revolutask.data.exception.ApiException
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CurrencyRepositoryImplTest {

    @Mock
    private lateinit var currencyRemoteDataSource: CurrencyRemoteDataSource

    private lateinit var currencyRepositoryImpl: CurrencyRepositoryImpl

    private lateinit var currencyListDataModel1: CurrencyListDataModel
    private lateinit var currencyListDataModel2: CurrencyListDataModel

    @Before
    fun setUp() {
        currencyRepositoryImpl = CurrencyRepositoryImpl(currencyRemoteDataSource)

        currencyListDataModel1 = CurrencyListDataModel(
            "EUR",
            1574501949484L,
            mapOf(
                "AUD" to 1.6097,
                "BGN" to 1.9477,
                "BRL" to 4.7719,
                "CAD" to 1.5274
            )
        )

        currencyListDataModel2 = CurrencyListDataModel("", 0L, emptyMap())
    }

    @Test
    fun testSuccessfulCases() {
        Mockito.`when`(currencyRemoteDataSource.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.just(currencyListDataModel1)
            )
        currencyRepositoryImpl.getLatestCurrencies("EUR")
            .test()
            .assertResult(
                currencyListDataModel1
            )
        Mockito.`when`(currencyRemoteDataSource.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.just(currencyListDataModel2)
            )
        currencyRepositoryImpl.getLatestCurrencies("EUR")
            .test()
            .assertResult(
                currencyListDataModel2
            )
    }

    @Test
    fun testErrorCases() {
        Mockito.`when`(currencyRemoteDataSource.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(Exception())
            )
        currencyRepositoryImpl.getLatestCurrencies("EUR")
            .test()
            .assertError(
                Exception::class.java
            )
        Mockito.`when`(currencyRemoteDataSource.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(TestException())
            )
        currencyRepositoryImpl.getLatestCurrencies("EUR")
            .test()
            .assertError(
                TestException::class.java
            )
        Mockito.`when`(currencyRemoteDataSource.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(ApiException.RequestFailedException(500))
            )
        currencyRepositoryImpl.getLatestCurrencies("EUR")
            .test()
            .assertError {
                it is ApiException.RequestFailedException &&
                        it.code == 500
            }
    }

    private class TestException : Exception()

}