package com.revolut.revolutask.domain.usecase

import com.revolut.revolutask.common.rx.TestSchedulerProvider
import com.revolut.revolutask.common.rx.TrampolineSchedulerProvider
import com.revolut.revolutask.data.exception.NoNetworkException
import com.revolut.revolutask.domain.exception.DomainException
import com.revolut.revolutask.domain.exception.InvalidBaseCurrencyException
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.PublishSubject
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.TimeUnit


@RunWith(MockitoJUnitRunner::class)
class UpdateCurrencyRatesUseCaseTest {

    @Mock
    private lateinit var getCurrencyRatesUseCase: GetCurrencyRatesUseCase

    private lateinit var updateCurrencyRatesUseCase: UpdateCurrencyRatesUseCase

    private lateinit var currencyListDataModel1: CurrencyListDataModel

    private lateinit var successSubject: PublishSubject<CurrencyListDataModel>
    private lateinit var errorSubject: PublishSubject<Throwable>

    private lateinit var testScheduler: TestScheduler

    private lateinit var successTestObserver: TestObserver<CurrencyListDataModel>
    private lateinit var errorTestObserver: TestObserver<Throwable>

    @Before
    fun setUp() {
        testScheduler = TestScheduler()
        successSubject = PublishSubject.create()
        errorSubject = PublishSubject.create()
        Mockito.`when`(getCurrencyRatesUseCase.successObservable)
            .thenReturn(successSubject)
        Mockito.`when`(getCurrencyRatesUseCase.errorObservable)
            .thenReturn(errorSubject)
        successTestObserver = TestObserver()
        errorTestObserver = TestObserver()

        updateCurrencyRatesUseCase = UpdateCurrencyRatesUseCase(
            getCurrencyRatesUseCase,
            TrampolineSchedulerProvider(),
            TestSchedulerProvider(testScheduler)
        )
        updateCurrencyRatesUseCase.successObservable.subscribeWith(successTestObserver)
        updateCurrencyRatesUseCase.errorObservable.subscribeWith(errorTestObserver)

        currencyListDataModel1 = CurrencyListDataModel(
            "EUR",
            1574501949484L,
            mapOf(
                "AUD" to 1.6097,
                "BGN" to 1.9477,
                "BRL" to 4.7719,
                "CAD" to 1.5274
            )
        )
    }

    @Test
    fun execute_shouldEmitSameParameters() {
        Mockito.`when`(getCurrencyRatesUseCase.execute("EUR"))
            .then {
                successSubject.onNext(currencyListDataModel1)
            }
        updateCurrencyRatesUseCase.execute("EUR")
        successTestObserver
            .assertNoValues()
        errorTestObserver
            .assertNoValues()

        testScheduler.advanceTimeBy(1L, TimeUnit.SECONDS)
        successTestObserver
            .assertValue(
                currencyListDataModel1
            )
        errorTestObserver
            .assertNoValues()
    }

    @Test
    fun execute_shouldEmitDomainException() {
        Mockito.`when`(getCurrencyRatesUseCase.execute("USD"))
            .then {
                errorSubject.onNext(DomainException())
            }

        updateCurrencyRatesUseCase.execute("USD")
        testScheduler.advanceTimeBy(1L, TimeUnit.SECONDS)
        successTestObserver
            .assertNoValues()
        errorTestObserver
            .assertValue {
                it is DomainException
            }
    }

    @Test
    fun execute_shouldEmitNoNetworkException() {
        Mockito.`when`(getCurrencyRatesUseCase.execute("USD"))
            .then {
                errorSubject.onNext(DomainException())
            }

        updateCurrencyRatesUseCase.execute("USD")
        testScheduler.advanceTimeBy(1L, TimeUnit.SECONDS)
        successTestObserver
            .assertNoValues()
        errorTestObserver
            .assertValue {
                it is DomainException
            }
    }

    @Test
    fun execute_shouldEmitMultipleValues() {
        Mockito.`when`(getCurrencyRatesUseCase.execute("EUR"))
            .then {
                successSubject.onNext(currencyListDataModel1)
            }

        updateCurrencyRatesUseCase.execute("EUR")
        successTestObserver
            .assertNoValues()
        errorTestObserver
            .assertNoValues()

        testScheduler.advanceTimeBy(1L, TimeUnit.SECONDS)
        successTestObserver
            .assertValue(
                currencyListDataModel1
            )
        errorTestObserver
            .assertNoValues()

        testScheduler.advanceTimeBy(1L, TimeUnit.SECONDS)
        successTestObserver
            .assertValueAt(
                1,
                currencyListDataModel1
            )
        errorTestObserver
            .assertNoValues()
    }

    @Test
    fun execute_andCancel() {
        Mockito.`when`(getCurrencyRatesUseCase.execute("EUR"))
            .then {
                successSubject.onNext(currencyListDataModel1)
            }

        updateCurrencyRatesUseCase.execute("EUR")
        testScheduler.advanceTimeBy(1L, TimeUnit.SECONDS)
        successTestObserver
            .assertValue(
                currencyListDataModel1
            )
        errorTestObserver
            .assertNoValues()

        updateCurrencyRatesUseCase.cancel()
        // Even though time is passed by 100 seconds, no additional data should be emitted
        testScheduler.advanceTimeBy(100L, TimeUnit.SECONDS)
        successTestObserver
            .assertValue(
                currencyListDataModel1
            )
        errorTestObserver
            .assertNoValues()
    }

    private class TestException : Exception()

}