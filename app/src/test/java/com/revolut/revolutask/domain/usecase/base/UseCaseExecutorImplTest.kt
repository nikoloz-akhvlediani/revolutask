package com.revolut.revolutask.domain.usecase.base

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UseCaseExecutorImplTest {

    @Mock
    private lateinit var useCase1: UseCase<Unit>
    @Mock
    private lateinit var useCase2: UseCase<Unit>
    @Mock
    private lateinit var useCase3: UseCase<Unit>

    private lateinit var useCaseExecutorImpl: UseCaseExecutorImpl

    @Before
    fun setUp() {
        useCaseExecutorImpl = UseCaseExecutorImpl()
    }

    /**
     * Execute use cases. See if UseCase.execute() is called
     * Check stack if it contains executed use cases
     */
    @Test
    fun executeUseCase() {
        useCaseExecutorImpl.executeUseCase(useCase1, Unit)
        Mockito.verify(useCase1, Mockito.times(1)).execute(Unit)
        assertTrue(useCaseExecutorImpl.stack.contains(useCase1))

        useCaseExecutorImpl.executeUseCase(useCase2, Unit)
        Mockito.verify(useCase2, Mockito.times(1)).execute(Unit)
        assertTrue(useCaseExecutorImpl.stack.contains(useCase2))

        assertTrue(useCaseExecutorImpl.stack.size == 2)
    }

    /**
     * Execute use cases and cancel them. See if UseCase.cancel() is called
     * Check if stack contains those use cases (it should!)
     */
    @Test
    fun cancelUseCase() {
        useCaseExecutorImpl.executeUseCase(useCase1, Unit)
        useCaseExecutorImpl.cancelUseCase(useCase1)
        Mockito.verify(useCase1, Mockito.times(1)).cancel()
        assertTrue(useCaseExecutorImpl.stack.contains(useCase1))

        useCaseExecutorImpl.executeUseCase(useCase2, Unit)
        useCaseExecutorImpl.cancelUseCase(useCase2)
        Mockito.verify(useCase2, Mockito.times(1)).cancel()
        assertTrue(useCaseExecutorImpl.stack.contains(useCase2))

        assertTrue(useCaseExecutorImpl.stack.size == 2)
    }

    /**
     * Execute use cases and clear all of them. See if UseCase.clear() is called
     * Check if stack contains those use cases (it should not!)
     */
    @Test
    fun clear() {
        useCaseExecutorImpl.executeUseCase(useCase1, Unit)
        useCaseExecutorImpl.executeUseCase(useCase2, Unit)
        useCaseExecutorImpl.executeUseCase(useCase3, Unit)

        useCaseExecutorImpl.clear()

        Mockito.verify(useCase1, Mockito.times(1)).clear()
        Mockito.verify(useCase2, Mockito.times(1)).clear()
        Mockito.verify(useCase3, Mockito.times(1)).clear()

        assertTrue(useCaseExecutorImpl.stack.isEmpty())
        assertFalse(useCaseExecutorImpl.stack.contains(useCase1))
        assertFalse(useCaseExecutorImpl.stack.contains(useCase2))
        assertFalse(useCaseExecutorImpl.stack.contains(useCase3))
    }

}