package com.revolut.revolutask.domain.usecase

import com.revolut.revolutask.data.exception.NoNetworkException
import com.revolut.revolutask.domain.exception.DomainException
import com.revolut.revolutask.domain.exception.InvalidBaseCurrencyException
import com.revolut.revolutask.domain.model.CurrencyListDataModel
import com.revolut.revolutask.domain.repository.CurrencyRepository
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetCurrencyRatesUseCaseTest {

    @Mock
    private lateinit var currencyRepository: CurrencyRepository

    private lateinit var useCase: GetCurrencyRatesUseCase

    private lateinit var currencyListDataModel1: CurrencyListDataModel
    private lateinit var currencyListDataModel2: CurrencyListDataModel
    private lateinit var currencyListDataModel3: CurrencyListDataModel


    @Before
    fun setUp() {
        useCase = GetCurrencyRatesUseCase(currencyRepository)

        currencyListDataModel1 = CurrencyListDataModel(
            "EUR",
            1574501949484L,
            mapOf(
                "AUD" to 1.6097,
                "BGN" to 1.9477,
                "BRL" to 4.7719,
                "CAD" to 1.5274
            )
        )

        currencyListDataModel2 = CurrencyListDataModel(
            "USD",
            1574501949484L,
            mapOf(
                "EUR" to 1.6097,
                "BRL" to 4.7719,
                "CAD" to 1.5274
            )
        )
        currencyListDataModel3 = CurrencyListDataModel("", 0L, emptyMap())
    }

    @Test
    fun execute_shouldEmitSameParameters() {
        Mockito.`when`(currencyRepository.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.just(currencyListDataModel1)
            )

        val successTestObserver = TestObserver<CurrencyListDataModel>()
        val errorTestObserver = TestObserver<Throwable>()
        useCase.successObservable.subscribeWith(successTestObserver)
        useCase.errorObservable.subscribeWith(errorTestObserver)

        useCase.execute("EUR")
        successTestObserver
            .assertValue(
                currencyListDataModel1
            )
        errorTestObserver
            .assertNoValues()
    }

    @Test
    fun execute_shouldEmitInvalidCurrencyBaseException() {
        Mockito.`when`(currencyRepository.getLatestCurrencies("USD"))
            .thenReturn(
                Single.just(currencyListDataModel1)
            )

        val successTestObserver = TestObserver<CurrencyListDataModel>()
        val errorTestObserver = TestObserver<Throwable>()
        useCase.successObservable.subscribeWith(successTestObserver)
        useCase.errorObservable.subscribeWith(errorTestObserver)

        useCase.execute("USD")
        successTestObserver
            .assertNoValues()
        errorTestObserver
            .assertValue {
                it is InvalidBaseCurrencyException
            }
    }

    @Test
    fun execute_shouldEmitDomainException() {
        Mockito.`when`(currencyRepository.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(TestException())
            )

        val successTestObserver = TestObserver<CurrencyListDataModel>()
        val errorTestObserver = TestObserver<Throwable>()
        useCase.successObservable.subscribeWith(successTestObserver)
        useCase.errorObservable.subscribeWith(errorTestObserver)

        useCase.execute("EUR")
        successTestObserver
            .assertNoValues()
        errorTestObserver
            .assertValue {
                it is DomainException
            }
    }

    @Test
    fun execute_shouldEmitNoNetworkException() {
        Mockito.`when`(currencyRepository.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.error(NoNetworkException())
            )

        val successTestObserver = TestObserver<CurrencyListDataModel>()
        val errorTestObserver = TestObserver<Throwable>()
        useCase.successObservable.subscribeWith(successTestObserver)
        useCase.errorObservable.subscribeWith(errorTestObserver)

        useCase.execute("EUR")
        successTestObserver
            .assertNoValues()
        errorTestObserver
            .assertValue {
                it is NoNetworkException
            }
    }

    @Test
    fun execute_shouldEmitMultipleValues() {
        Mockito.`when`(currencyRepository.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.just(currencyListDataModel1)
            )
        Mockito.`when`(currencyRepository.getLatestCurrencies("USD"))
            .thenReturn(
                Single.just(currencyListDataModel2)
            )

        val successTestObserver = TestObserver<CurrencyListDataModel>()
        val errorTestObserver = TestObserver<Throwable>()
        useCase.successObservable.subscribeWith(successTestObserver)
        useCase.errorObservable.subscribeWith(errorTestObserver)

        useCase.execute("EUR")
        successTestObserver
            .assertValue(
                currencyListDataModel1
            )
        errorTestObserver
            .assertNoValues()

        useCase.execute("USD")
        successTestObserver
            .assertValueAt(
                1,
                currencyListDataModel2
            )
        errorTestObserver
            .assertNoValues()
    }

    @Test
    fun execute_shouldEmitValuesAndErrors() {
        Mockito.`when`(currencyRepository.getLatestCurrencies("EUR"))
            .thenReturn(
                Single.just(currencyListDataModel1)
            )
        Mockito.`when`(currencyRepository.getLatestCurrencies("USD"))
            .thenReturn(
                Single.just(currencyListDataModel2)
            )

        val successTestObserver = TestObserver<CurrencyListDataModel>()
        val errorTestObserver = TestObserver<Throwable>()
        useCase.successObservable.subscribeWith(successTestObserver)
        useCase.errorObservable.subscribeWith(errorTestObserver)

        useCase.execute("EUR")
        successTestObserver
            .assertValue(
                currencyListDataModel1
            )
        errorTestObserver
            .assertNoValues()

        useCase.execute("USD")
        successTestObserver
            .assertValueAt(
                1,
                currencyListDataModel2
            )

        Mockito.`when`(currencyRepository.getLatestCurrencies("ASDASD"))
            .thenReturn(
                Single.error(Exception())
            )
        useCase.execute("ASDASD")
        errorTestObserver
            .assertValue {
                it is DomainException
            }
    }

    companion object {

        @BeforeClass
        @JvmStatic
        fun before() {
            RxAndroidPlugins.reset()
            RxJavaPlugins.reset()
            RxJavaPlugins.setIoSchedulerHandler {
                return@setIoSchedulerHandler Schedulers.trampoline()
            }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler {
                return@setInitMainThreadSchedulerHandler Schedulers.trampoline()
            }
        }

        @AfterClass
        @JvmStatic
        fun after() {
            RxAndroidPlugins.reset()
            RxJavaPlugins.reset()
        }
    }

    private class TestException : Exception()

}